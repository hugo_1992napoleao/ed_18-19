package Resources;

public class ElementNotFoundException extends Exception {
    /**
     * Exception when the element is not found
     * @param msg - String message you want to display
     */
    public ElementNotFoundException (String msg) {
        System.out.println(msg);
    }
}

package Resources;

public interface IUnorderedListADT<T> extends IListADT<T> {
    /**
     * Adds a element to the front of the list
     *
     * @param elemet - element to be added
     */
    public void addToFront(Object elemet);

    /**
     * Adds a element to de rear of the list
     *
     * @param element - element to be added
     */
    public void addToRear(Object element);

    /**
     * Adds a element after a chosen element
     *
     * @param element - element to be added
     * @param target  - element to bo added after
     */
    public void addAfter(Object element, Object target);
}

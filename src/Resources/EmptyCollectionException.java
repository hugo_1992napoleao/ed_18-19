package Resources;

public class EmptyCollectionException extends Exception {
    /**
     * Method that is send when the collection is empty
     * @param stack - String message you want to display
     */
    public EmptyCollectionException(String stack) {
        System.out.println(stack);
    }
}

package List;

import Resources.IUnorderedListADT;

public class UnorderedArrayList extends ArrayList implements IUnorderedListADT {
    /**
     * Adds a element to the front of the list
     *
     * @param elemet - element to be added
     */
    @Override
    public void addToFront(Object elemet) {
        if (size() == array.length)
            extendCapacity();
        for (int scan = tail; scan > 0; scan--)
            array[scan] = array[scan - 1];
        array[0] = elemet;
        tail++;
        count++;
    }

    /**
     * Adds a element to de rear of the list
     *
     * @param element - element to be added
     */
    @Override
    public void addToRear(Object element) {
        if (size() == this.array.length)
            extendCapacity();
        this.array[this.tail] = element;
        this.count++;
        this.tail = this.tail + 1 % array.length;
    }

    /**
     * Adds a element after a chosen element
     *
     * @param element - element to be added
     * @param target  - element to bo added after
     */
    @Override
    public void addAfter(Object element, Object target) {

    }
}

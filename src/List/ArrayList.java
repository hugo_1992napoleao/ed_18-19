package List;

import Resources.ElementNotFoundException;
import Resources.EmptyCollectionException;
import Resources.IListADT;

import java.util.Iterator;

public class ArrayList<T> implements IListADT<T> {

    protected static final int capacity = 100;
    protected T[] array;
    protected int head;
    protected int tail;
    protected int count;

    public ArrayList() {
        this.array = (T[]) new Object[this.capacity];
        this.tail = 0;
        this.head = 0;
        this.count = 0;
    }

    public ArrayList(int inicialCapacity) {
        this.array = (T[]) new Object[inicialCapacity];
        this.tail = 0;
        this.head = 0;
        this.count = 0;
    }

    public void extendCapacity() {
        //TODO: acabar!
    }

    /**
     * Removes and returns the first element from this list.
     *
     * @return the first element from this list
     */
    @Override
    public T removeFirst() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("A lista esta vazia");
        T resul = this.array[this.head];
        this.array[this.head] = null;
        this.head = this.head + 1 % array.length;
        this.count--;
        return resul;
    }

    /**
     * Removes and returns the last element from this list.
     *
     * @return the last element from this list
     */
    @Override
    public T removeLast() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("A lista esta vazia");
        T resul = this.array[this.tail];
        this.array[this.tail] = null;
        this.tail = this.tail + 1 % array.length;
        this.count--;
        return resul;
    }

    /**
     * Removes and returns the specified element from this list.
     *
     * @param element the element to be removed from the list
     */
    @Override
    public T remove(T element) throws EmptyCollectionException, ElementNotFoundException {
        if (size() == 0)
            throw new EmptyCollectionException("A coleção esta vazia");
        boolean found = false;
        int pos = -1;
        T resul = null;

        for (int i = this.head; i < this.tail; i++) {
            if (element.equals(array[i])) {
                found = true;
                pos = i;
                resul = this.array[i];
            }
        }

        if (found == false)
            throw new ElementNotFoundException("O elemento não foi encontrado na coleção");

        for (int i = this.head; i < pos; i++) {
            this.array[i] = this.array[i + 1];
        }
        this.head = this.head + 1 % array.length;
        this.count--;
        return resul;
    }

    /**
     * Returns a reference to the first element in this list.
     *
     * @return a reference to the first element in this list
     */
    @Override
    public T first() {
        return this.array[this.head];
    }

    /**
     * Returns a reference to the last element in this list.
     *
     * @return a reference to the last element in this list
     */
    @Override
    public T last() {
        return this.array[this.tail];
    }

    /**
     * Returns true if this list contains the specified target
     * element.
     *
     * @param target the target that is being sought in the list
     * @return true if the list contains this element
     */
    @Override
    public boolean contains(T target) {
        boolean found = false;

        for (int i = this.head; i < this.tail; i++) {
            if (this.array[i].equals(target)) {
                found = true;
            }
        }
        return found;
    }

    /**
     * Returns true if this list contains no elements.
     *
     * @return true if this list contains no elements
     */
    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * Returns the number of elements in this list.
     *
     * @return the integer representation of number of
     * elements in this list
     */
    @Override
    public int size() {
        return this.count;
    }

    /**
     * Returns an iterator for the elements in this list.
     *
     * @return an iterator over the elements in this list
     */
    @Override
    public Iterator<T> iterator() {
        BasicIterator basicIterator = new BasicIterator();
        return basicIterator;
    }

    public class BasicIterator implements Iterator {

        private T[] basicArray = array;
        private int basicCount = count;

        @Override
        public boolean hasNext() {
            return this.basicCount == array.length;
        }

        @Override
        public Object next() {
            Object resul = this.basicArray[this.basicCount];
            this.basicCount++;
            return resul;
        }

        @Override
        public void remove() {

        }
    }
}

package List;

import Resources.IOrderedListADT;

public class ArrayOrderedList extends ArrayList implements IOrderedListADT {

    public ArrayOrderedList() {
        super();
    }

    public ArrayOrderedList(int inicialCapacity) {
        super(inicialCapacity);
    }

    /**
     * Adds the specified element to this list at
     * the proper location
     *
     * @param element the element to be added to this list
     */
    @Override
    public void add(Object element) {
        if (size() == array.length) {
            extendCapacity();
        }
        Comparable temp = (Comparable) element;

        int scan = 0;
        while (scan < tail && temp.compareTo(array[scan]) > 0) {
            scan++;
        }

        for (int scan2 = tail; scan2 > scan; scan2--) {
            array[scan2] = array[scan2 - 1];
        }
        array[scan] = element;
        tail++;
    }
}

package List;

import Resources.ElementNotFoundException;
import Resources.EmptyCollectionException;
import Resources.IListADT;
import Resources.LinearNode;

import java.util.Iterator;

public class LinkedList<T> implements IListADT<T> {
    protected LinearNode<T> head;
    protected LinearNode<T> tail;
    protected int count;

    /**
     * Creates a empty linked list
     */
    public LinkedList() {
        this.head = new LinearNode<>();
        this.tail = new LinearNode<>();
        this.count = 0;
    }

    @Override
    public T removeFirst() {
        T resul = this.head.getElement();
        this.head = this.head.getNext();
        return resul;
    }

    @Override
    public T removeLast() {
        T resul = this.tail.getElement();
        LinearNode<T> previus = null;
        LinearNode<T> current = this.head;

        while (current != null) {
            if (current.getNext() == null) {
                current.setNext(null);
            } else {
                current = current.getNext();
            }
        }
        return resul;
    }

    @Override
    public Object remove(Object element) throws EmptyCollectionException, ElementNotFoundException {
        if (isEmpty()) {
            throw new EmptyCollectionException("A lista esta vazia");
        }

        boolean found = false;
        LinearNode<T> previous = null;
        LinearNode<T> current = this.head;

        while (current != null && !found) {
            if (element.equals(current.getElement())) {
                found = true;
            } else {
                previous = current;
                current = current.getNext();
            }
        }
        if (!found) {
            throw new ElementNotFoundException("O elemento não foi encontrado");
        }

        if (size() == 1) {
            this.head = this.tail = null;
        } else if (current.equals(this.head)) {
            this.head = current.getNext();
        } else if (current.equals(this.tail)) {
            this.tail = previous;
            this.tail.setNext(null);
        } else {
            previous.setNext(current.getNext());
        }

        this.count--;

        return current.getElement();

    }

    @Override
    public T first() {
        return this.head.getElement();
    }

    @Override
    public T last() {
        return tail.getElement();
    }

    @Override
    public boolean contains(Object target) {
        boolean found = false;
        LinearNode<T> previous = null;
        LinearNode<T> current = this.head;
        while (current != null && !found) {
            if (target.equals(current.getElement())) {
                found = true;
            } else {
                current = current.getNext();
            }
        }
        return found;
    }

    @Override
    public boolean isEmpty() {
        return this.count == 0;
    }

    @Override
    public int size() {
        return this.count;
    }

    @Override
    public Iterator iterator() {
        BasicIterator basicIterator = new BasicIterator();
        return basicIterator;
    }

    @Override
    public String toString() {
        String result = "";
        LinearNode<T> current = this.head;

        while (current != null) {
            result = result + (current.getElement()).toString() + "\n";
            current = current.getNext();
        }

        return result;
    }

    public class BasicIterator implements Iterator {

        private LinearNode node = head;

        @Override
        public boolean hasNext() {
            return this.node != null;
        }

        @Override
        public Object next() {
            Object resul = this.node.getElement();
            this.node = this.node.getNext();
            return resul;
        }

        @Override
        public void remove() {

        }
    }
}

package Graph;

import Resources.ElementNotFoundException;
import Resources.EmptyCollectionException;
import Resources.GraphADT;

import java.util.Iterator;

public class ArrayGraph<T> implements GraphADT {
    protected final int DEFAULT_CAPACITY = 10;
    protected int numVertices;   // number of vertices in the graph
    protected boolean[][] adjMatrix;   // adjacency matrix
    protected T[] vertices;   // values of vertices

    /**
     * Adds a vertex to this graph, associating object with vertex.
     *
     * @param vertex the vertex to be added to this graph
     */
    @Override
    public void addVertex(Object vertex) {
        if (vertices.length == numVertices) {
            expandCapacity();
        }
        this.vertices[this.numVertices] = (T) vertex;
        this.numVertices++;
    }

    private void expandCapacity() {
        T[] temp = (T[]) new Object[this.vertices.length * 2];
        for (int i = 0; i < this.numVertices; i++) {
            temp[i] = this.vertices[i];
        }
        this.vertices = temp;
    }

    /**
     * Removes a single vertex with the given value from this graph.
     *
     * @param vertex the vertex to be removed from this graph
     */
    @Override
    public void removeVertex(Object vertex) throws EmptyCollectionException, ElementNotFoundException {
        if (isEmpty()) {
            throw new EmptyCollectionException("Graph vazio");
        }
        int position;

        position = findElement(vertex);

        if (position == -1) {
            throw new ElementNotFoundException("O elemento não foi encontrado");
        }

        if (this.numVertices - position >= 0)
            System.arraycopy(this.vertices, position + 1, this.vertices, position, this.numVertices - position);
        this.numVertices--;
    }

    private int findElement(Object vertex) {
        int aux = -1;

        for (int i = 0; i < this.numVertices; i++) {
            if (this.vertices[i].equals(vertex)) {
                aux = i;
            }
        }
        return aux;
    }

    /**
     * Inserts an edge between two vertices of this graph.
     *
     * @param vertex1 the first vertex
     * @param vertex2 the second vertex
     */
    @Override
    public void addEdge(Object vertex1, Object vertex2) throws ElementNotFoundException {
        int pos1, pos2;

        pos1 = findElement(vertex1);
        pos2 = findElement(vertex2);

        if (pos1 == -1 || pos2 == -1) {
            throw new ElementNotFoundException("Elemento não encontrado");
        }

        this.adjMatrix[pos1][pos2] = true;
        this.adjMatrix[pos2][pos1] = true;
    }

    /**
     * Removes an edge between two vertices of this graph.
     *
     * @param vertex1 the first vertex
     * @param vertex2 the second vertex
     */
    @Override
    public void removeEdge(Object vertex1, Object vertex2) throws ElementNotFoundException {
        int pos1, pos2;

        pos1 = findElement(vertex1);
        pos2 = findElement(vertex2);

        if (pos1 == -1 || pos2 == -1) {
            throw new ElementNotFoundException("Elemento não encontrado");
        }

        this.adjMatrix[pos1][pos2] = false;
        this.adjMatrix[pos2][pos1] = false;
    }

    /**
     * Returns a breadth first iterator starting with the given vertex.
     *
     * @param startVertex the starting vertex
     * @return a breadth first iterator beginning at
     * the given vertex
     */
    @Override
    public Iterator iteratorBFS(Object startVertex) {
        return null;
    }

    /**
     * Returns a depth first iterator starting with the given vertex.
     *
     * @param startVertex the starting vertex
     * @return a depth first iterator starting at the
     * given vertex
     */
    @Override
    public Iterator iteratorDFS(Object startVertex) {
        return null;
    }

    /**
     * Returns an iterator that contains the shortest path between
     * the two vertices.
     *
     * @param startVertex  the starting vertex
     * @param targetVertex the ending vertex
     * @return an iterator that contains the shortest
     * path between the two vertices
     */
    @Override
    public Iterator iteratorShortestPath(Object startVertex, Object targetVertex) {
        return null;
    }

    /**
     * Returns true if this graph is empty, false otherwise.
     *
     * @return true if this graph is empty
     */
    @Override
    public boolean isEmpty() {
        return this.numVertices == 0;
    }

    /**
     * Returns true if this graph is connected, false otherwise.
     *
     * @return true if this graph is connected
     */
    @Override
    public boolean isConnected() {
        return true;
    }

    /**
     * Returns the number of vertices in this graph.
     *
     * @return the integer number of vertices in this graph
     */
    @Override
    public int size() {
        return this.numVertices;
    }
}

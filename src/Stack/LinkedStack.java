package Stack;

import Resources.EmptyCollectionException;
import Resources.IStackADT;
import Resources.LinearNode;

public class LinkedStack<T> implements IStackADT {
    private LinearNode top;
    private int count;

    public LinkedStack() {
        this.top = new LinearNode();
        this.count = 0;
    }

    @Override
    public void push(Object element) {

        if (isEmpty()) {
            this.top.setElement(element);
            this.count++;
        } else {
            LinearNode temp = new LinearNode();
            temp.setElement(element);
            temp.setNext(this.top);
            this.top = temp;
            this.count++;
        }
    }

    @Override
    public Object pop() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("A Stack esta vazia");
        Object temp = this.top.getElement();
        this.top = this.top.getNext();
        this.count--;
        return temp;
    }

    @Override
    public Object peek() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("A Stack esta vazia");
        return this.top.getElement();
    }

    @Override
    public boolean isEmpty() {
        return (size() == 0) ? true : false;
    }

    @Override
    public int size() {
        return this.count;
    }

    @Override
    public String toString() {
        return "LinkedStack{" +
                "top=" + top +
                ", count=" + count +
                '}';
    }
}

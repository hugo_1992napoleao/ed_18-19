package Stack;

import Resources.EmptyCollectionException;
import Resources.IStackADT;

import java.util.Arrays;

public class ArrayStack<T> implements IStackADT {
    /**
     * constant to represent the default capacity of the array
     */
    private final int DEFAULT_CAPACITY = 100;
    /**
     * int that represents both the number of elements and the next
     * available position in the array
     */
    private int top;
    /**
     * array of generic elements to represent the stack
     */
    private T[] stack;
    /**
     * Creates an empty stack using the default capacity.
     */
    public ArrayStack()
    {
        top = 0;
        stack = (T[])(new Object[DEFAULT_CAPACITY]);
    }
    /**
     * Creates an empty stack using the specified capacity.
     * @param initialCapacity represents the specified capacity
     */
    public ArrayStack(int initialCapacity)
    {
        top = 0;
        stack = (T[])(new Object[initialCapacity]);
    }
    @Override
    public void push(Object element) {
        if (size() == stack.length)
            expandCapacity();
        stack[top] = (T) element;
        top++;
    }

    private void expandCapacity() {
        T [] temp = (T[]) (new Object[size() * 2]);
        temp = this.stack;
        this.stack = temp;
    }

    @Override
    public Object pop() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("Stack");
        top--;
        T result = stack[top];
        stack[top] = null;
        return result;
    }

    @Override
    public Object peek() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("Stack");
        return stack[top-1];
    }

    @Override
    public boolean isEmpty() {
        return size() == 0 ? true : false;
    }

    @Override
    public int size() {
        return this.top;
    }

    @Override
    public String toString() {
        return "ArrayStack{" +
                "top=" + top +
                ", stack=" + Arrays.toString(stack) +
                '}';
    }
}

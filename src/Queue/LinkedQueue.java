package Queue;

import Resources.EmptyCollectionException;
import Resources.IQueueADT;
import Resources.LinearNode;

public class LinkedQueue<T> implements IQueueADT {
    private LinearNode front;
    private LinearNode rear;
    private int count;

    public LinkedQueue() {
        this.front = new LinearNode();
        this.rear = new LinearNode();
        this.count = 0;
    }

    @Override
    public void enqueue(Object element) {
        LinearNode node = new LinearNode(element);
        if (isEmpty()) {
            this.front = node;
        } else {
            this.rear.setNext(node);
        }
        this.rear = node;
        this.count++;
    }

    @Override
    public Object dequeue() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("A queue esta vazia");
        Object temp = this.rear.getElement();
        this.front = this.front.getNext();
        this.count--;
        return temp;
    }

    @Override
    public Object first() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("A queue esta vazia");
        return this.rear.getElement();
    }

    @Override
    public boolean isEmpty() {
        return size() == 0 ? true : false;
    }

    @Override
    public int size() {
        return this.count;
    }

    @Override
    public String toString() {
        String result = "";
        LinearNode<T> current = front;

        while (current != null) {
            result = result + (current.getElement()).toString() + "\n";
            current = current.getNext();
        }

        return result;
    }
}

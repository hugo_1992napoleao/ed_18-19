package Queue;

import Resources.EmptyCollectionException;
import Resources.IQueueADT;

public class CircularArrayQueue<T> implements IQueueADT<T> {
    private static final int capacity = 100;
    private T[] array;
    private int head;
    private int tail;
    private int count;

    public CircularArrayQueue() {
        this.array = (T[]) new Object[this.capacity];
        this.head = 0;
        this.tail = 0;
        this.count = 0;
    }

    public CircularArrayQueue(int inicialCapacity) {
        this.array = (T[]) new Object[inicialCapacity];
        this.head = 0;
        this.tail = 0;
        this.count = 0;
    }

    /**
     * Adds one element to the rear of this queue.
     *
     * @param element the element to be added to the rear of this queue
     */
    @Override
    public void enqueue(T element) {
        if (size() == this.array.length - 1)
            expandSize();
        this.array[this.tail] = element;
        this.tail = this.tail + 1 % array.length;
        this.count++;
    }

    /**
     * Removes and returns the element at the front of his queue.
     *
     * @return the element at the front of this queue
     */
    @Override
    public T dequeue() throws EmptyCollectionException {
        if (size() == 0)
            throw new EmptyCollectionException("A colação esta vazia");
        T result = this.array[this.head];
        this.head = this.head + 1 % array.length;
        this.count--;
        return result;
    }

    /**
     * Methode responsable for increasing the current size of the array
     */
    private void expandSize() {
        Object[] temp = new Object[this.array.length * 2];
        this.array = (T[]) temp;
    }

    /**
     * Returns without removing the element at the front of this queue.
     *
     * @return the first element in this queue
     */
    @Override
    public T first() throws EmptyCollectionException {
        return this.array[this.head];
    }

    /**
     * Returns true if this queue contains no elements.
     *
     * @return true if this queue is empty
     */
    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * Returns the number of elements in this queue.
     *
     * @return the integer representation of the size
     * of this queue
     */
    @Override
    public int size() {
        return this.count;
    }

    public String toString() {
        StringBuilder resul = new StringBuilder();

        for (int i = this.head; i < this.tail; i++) {
            resul.append(array[i]).append("\n");
        }

        return resul.toString();
    }
}
